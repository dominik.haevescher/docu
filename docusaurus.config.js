// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Docu',
  tagline: 'Dinosaurs are cool',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],
  plugins: [require.resolve("@cmfcmf/docusaurus-search-local")],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Tutoolio ',
        logo: {
          alt: 'Tutoolio',
          src: 'img/logo_tutoolio.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Shop',
          },
        ],

      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Shop',
                to: '/docs/intro',
              },
              {
                label: 'Int API',
                href: 'https://api.int-tutool.io/lms/swagger-ui/index.html?configUrl=/lms/api-docs/swagger-config',
              },
              {
                label: 'Uat API',
                href: 'https://api.ut-tutool.io/lms/swagger-ui/index.html?configUrl=/lms/api-docs/swagger-config',
              },
              {
                label: 'Prod API',
                href: 'https://api.tutool.io/lms/swagger-ui/index.html?configUrl=/lms/api-docs/swagger-config',
              },

            ],
          },
          {
            title: 'Links',
            items: [
              {
                label: 'LMS',
                href: 'https://admin.tutool.io',
              },
              {
                label: 'LMS Uat',
                href: 'https://admin.uat-tutool.io',
              },
              {
                label: 'LMS Int',
                href: 'https://admin.int-tutool.io',
              },
              {
                label: 'LMS Int Shop',
                href: 'https://shop.int-tutool.io',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Homepage',
                href: 'https://tutool.io/',
              },
              {
                label: 'Markplatz',
                href: 'https://healthcare.marktplatz-tutool.io/',
              },
            ],
          },
        ],
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
