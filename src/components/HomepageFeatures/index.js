import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
    {
        title: 'LMS REST Api',
        Svg: require('@site/static/img/tut-admin.svg').default,
        description: (
            <>
                REST API from the LMS core
            </>
        ),
    },
    {
        title: 'Shop Documentation',
        Svg: require('@site/static/img/tut-shop.svg').default,
        description: (
            <>
                Shop Documentation
            </>
        ),
    }
];

function Feature({Svg, title, description}) {
    return (
        <div className={clsx('col col--6')}>
            <div className="text--center">
                <Svg className={styles.featureSvg} role="img"/>
            </div>
            <div className="text--center padding-horiz--md">
                <h3>{title}</h3>
                <p>{description}</p>
            </div>
        </div>
    );
}

export default function HomepageFeatures() {
    return (
        <section className={styles.features}>
            <div className="container">
                <div className="row">
                    <div className="col col--10 col--offset-1">
                        <div className="row">
                            {FeatureList.map((props, idx) => (
                                <Feature key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
