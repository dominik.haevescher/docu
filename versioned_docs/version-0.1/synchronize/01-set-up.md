---
sidebar_position: 1
---

# Übersicht
:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Während der Entwicklung kann es zu breaking changes
kommen.

:::

### **Der gesamte Abschnitt gilt nur für Tutoolio Produkte**  
Damit Kurse aus dem LMS heraus synchronisiert werden können, benötigt man einen API Key um die Rechte zu erhalten, Kurse
von einem Managemenet Level zu synchronisieren und sie als Produkte erstellen zu können und diese auch verkaufen zu können.
Ein Verkauf eines Kurses im Shop bedeutet auch ein gleichzeitiges ausliefern des Kurses im LMS.


## API Key
Je nachdem aus welchem Tenant oder Instanz die Kurse als Basis dienen sollen, um im Shop als Produkt erstellt zu werden
muss auf dieser Ebene ein Api Key erstellt werden. Diesen API Key kann man in den jeweiligen Einstellungen erstellen
und anzeigen lassen.  

Nachdem man sich diesen Key erstellt hat, muss man diesen im jeweiligen Shop einsetzen. Hierzu öffnet man die `Tutoolio LMS Settings`
und den Reiter `Settings` und setzt dort im Feld Secret-Key den vorgesehenen Schlüssel.

![Api Key](/img/synchronize-api-key.png)


## Manuelle Synchronisation einrichten

In den Einstellungen `Tutoolio LMS Settings` im Reiter `Synchronization` ist man in der Lage die Kurse manuell zu Synchronisieren. 
Dazu betätigt man den Button `Load courses` um die Kurse aus dem LMS zu laden. Im nächsten Schritt folgt dann das erstellen der Kurse
als Produkte in dem man einzelne Kurse synchronisieren lässt, oder alle.

![Wordpress Settings](/img/synchronize.png)

## Automatisierte Synchronisation

`WIP`

