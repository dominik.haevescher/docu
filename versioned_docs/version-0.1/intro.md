---
sidebar_position: 1
---

# Übersicht

:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Während der Entwicklung kann es zu breaking changes
kommen.

:::

Hier wird der Prozess beschrieben, um ein Produkt im Tutoolio Shop zu erstellen, aktualisieren und zu löschen.
Es werden die Synchronisationseinstellungen erklärt und es wird auf die verschiedenen Produktkategorien eingegangen, 
welche zur Folge unterschiedliche Kaufprozesse beinhalten.

## Tutoolio Produkte

Tutoolio Produkte werden manuell oder automatisiert im Shop synchronisiert. Der Kaufprozess wird auch im Shop abgeschlossen
und die Kurse werden danach automatisiert an den Kunden im LMS ausgeliefert.

## Semiro Produkte

Bei Semiro Produkten handelt es sich um Produkte die nicht im LMS ausgeliefert werden. Die Synchronisation folgt über
die REST API vom Shop. Dabei ist Semiro dafür verantwortlich, dass die Produkte aktuell gehalten werden. Während des Kaufprozesses wird der Kunde
auf die Seite von Semiro weitergeleitet, der eigentliche Kauf wird nicht im Shop abgeschlossen. Weitere Details zum
Bestellprozess wird [hier](/docs/purchase/purchase#semiro-produkt) beschrieben.


## SAP Produkte

Bei SAP Produkten handelt es sich um Produkte die im LMS ausgeliefert werden, aber der Bestellprozess extern abgewickelt
wird. Produkte werden über die REST API erstellt, aktualisiert und gelöscht. Die Bestellinformationen werden nach erfolgreichem
Kauf an die REST API übermittelt und dann folgt eine automatisierte auslieferung der Kurse im LMS.
