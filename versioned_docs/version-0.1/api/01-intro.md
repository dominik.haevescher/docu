---
sidebar_position: 1
---

# Übersicht
:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Während der Entwicklung kann es zu breaking changes
kommen.

:::

## API Key
Um die Kommunikation mit der REST API zu ermöglichen benötigt man zuerst einen API KEY, die Erstellung des API KEYs
wird [hier](https://woocommerce.com/document/woocommerce-rest-api/#section-2) beschrieben.

## Testumgebung

Je nachdem auf welcher Umgebung man sich befindet ändern sich URLs und Features. Es gibt eine `int` Umgebung, 
die den jeweiligen development Status wiedergibt. Eine `uat` Umgebung auf der die neusten Features auf stabilität hin
getestet werden und die Produktivumgebung `prod`

* `int` - `https://shop.int-tutool.io/` - Development
* `uat` - `https://shop.uat-tutool.io/` - Akzeptanztest
* `prod` - `https://shop.tutool.io/` - Produktivumgebung